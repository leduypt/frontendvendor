export default function ({ store, redirect }) {
    // If the _user is not authenticated
    var auth = localStorage.getItem('user-token');
    if (!auth) {
      return redirect('/')
    }
  }