export default function ({ store, redirect }) {
    // If the _user is authenticated redirect to home page
    var auth = localStorage.getItem('user-token');
    if (auth) {
      return redirect('/dashboard')
    }
}