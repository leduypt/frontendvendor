import Vue from 'vue'
import Notify from 'vuejs-notification'

Vue.use(Notify, {visibility: 5000, permanent: true})