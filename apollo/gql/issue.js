import gql from "graphql-tag";

// GraphQL query

export const ISSUE_QUERY = gql` 
query getAllIssue($page: Int!, $limit: Int!, $search: String!) {
    getAllIssue(page: $page, limit: $limit, search: $search){
        id
        vendorIssueName
        vendorLabels
        vendorIssueDescription
        vendorIssueStatus
        vendorID
        isDeadline
        vendorLabels
        isDate
        isDateDone
        total
        authorID
        assignID
        issueDirection
        vendor{
            id
            vendorName
            vendorRate
            vendorPhone
            vendorNote
        }
        author{
            id
            email
            name
        }
        authorAssign{
            id
            email
            name
        }
    }
}`;

export const ISSUE_VIEW = gql `
query getIssueByID($id: String!) {
    getIssueByID (id: $id) {
        id
        vendorIssueName
        vendorLabels
        vendorIssueDescription
        vendorIssueStatus
        isDeadline
        issueDirection
        isDateDone
        isDate
        vendorID
        authorID
        assignID
        attachFile
        vendor{
            id
            vendorName
            vendorRate
            vendorPhone
            vendorNote
        }
        author{
            id
            email
            name
        }
        authorAssign{
            id
            email
            name
        }
    }
}`;

export const ISSUE_VIEW_BY_VENDOR = gql `
query getIssueByVendor($id: String!, $assign: String!, $search: String!, $startdate: String!, $enddate: String!, $status: String! ) {
    getIssueByVendor (id: $id, assign: $assign, search: $search, startdate: $startdate, enddate: $enddate, status: $status) {
        id
        vendorIssueName
        vendorLabels
        vendorIssueDescription
        vendorIssueStatus
        issueDirection
        isDeadline
        isDateDone
        isDate
        vendorID
        authorID
        assignID
        attachFile
        vendor{
            id
            vendorName
            vendorRate
            vendorPhone
            vendorNote
        }
        author{
            id
            email
            name
        }
        authorAssign{
            id
            email
            name
        }
    }
}`;

export const ALL_ISSUE_QUERY = gql` 
query allIssue{
    AllIssue{
        id
        vendorIssueName
        vendorLabels
        vendorIssueDescription
        isDate
        isDeadline
        issueDirection
        authorID
        assignID
        attachFile
        vendor{
            id
            vendorName
            vendorRate
            vendorPhone
            vendorNote
        }
        author{
            id
            email
            name
        }
        authorAssign{
            id
            email
            name
        }
    }
}`;


export const CREATE_ISSUE = gql`
mutation addIssue($vendorIssueName: String!, $vendorIssueDescription: String!, $vendorID: String!, $isDeadline: String!, $vendorLabels: String!, $vendorIssueStatus: statusTypeEnum!, $isDate: String!, $isDateDone: String!, $authorID: String!, $assignID: String!, $attachFile: String!, $issueDirection: String!) {
    addIssue(vendorIssueName: $vendorIssueName, vendorIssueDescription: $vendorIssueDescription, vendorID: $vendorID, isDeadline: $isDeadline, vendorLabels: $vendorLabels, vendorIssueStatus: $vendorIssueStatus, isDate: $isDate, isDateDone: $isDateDone, authorID: $authorID, assignID: $assignID, attachFile: $attachFile, issueDirection: $issueDirection) {
        id
        vendorIssueName
        vendorLabels
        vendorIssueDescription
        vendorIssueStatus
        vendorID
        issueDirection
        isDeadline
        vendorLabels
        isDate
        isDateDone
        authorID
        assignID
    }
}
`;

export const DELETE_ISSUE = gql`
mutation deleteIssue($id:String!){
    deleteIssue(id: $id){
        message
        type
    }
}
`;

export const UPDATE_ISSUE = gql`
mutation updateIssue($id: String!, $vendorIssueName: String!, $vendorIssueDescription: String!, $vendorID: String!, $isDeadline: String!, $vendorLabels: String!, $vendorIssueStatus: statusTypeEnum!, $isDate: String!, $isDateDone: String!, $authorID: String!, $assignID: String!, $attachFile: String!,  $issueDirection: String!) {
    updateIssue(id: $id, vendorIssueName: $vendorIssueName, vendorIssueDescription: $vendorIssueDescription, vendorID: $vendorID, isDeadline: $isDeadline, vendorLabels: $vendorLabels, vendorIssueStatus: $vendorIssueStatus, isDate: $isDate, isDateDone: $isDateDone, authorID: $authorID , assignID: $assignID, attachFile: $attachFile, issueDirection: $issueDirection) {
        id
        vendorIssueName
        vendorLabels
        issueDirection
        vendorIssueDescription
        vendorIssueStatus
        vendorID
        isDeadline
        vendorLabels
        isDate
        isDateDone
        authorID
        assignID
    }
}
`;