import gql from 'graphql-tag';

export const LOGIN = gql`
  mutation loginUser($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      id
      token
      name
      email
      createdAt
    }
  }
`;

export const QUERY_USER = gql`
query me{
  me{
    id
    name
    email
  }
}`;

export const ALL_USER = gql`
query users{
  users{
    id
    name
    email
  }
}`;


export const UPDATE_USER = gql`
  mutation updateUser($name: String!, $email: String!) {
    updateUser(name: $name, email: $email) {
      id
      name
      email
    }
  }
`;