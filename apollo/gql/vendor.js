import gql from "graphql-tag";

// GraphQL query

export const VENDOR_QUERY = gql` 
query getAllVendor($page: Int!, $limit: Int!) {
    getAllVendor(page: $page, limit: $limit){
        id
        vendorName
        vendorInfo
        vendorType
        vendorDauMoi
        vendorPhone
        vendorType
        vendorNote
        startDate
        vendorRate
        vendorIssue{
            id
            vendorIssueName
            vendorIssueStatus
            vendorIssueDescription
            vendorLabels
            issueDirection
        }
    }
}`;

export const VENDOR_VIEW = gql `
query getVendorByID($id: String!) {
    getVendorByID (id: $id) {
        id
        vendorName
        vendorInfo
        vendorType
        vendorDauMoi
        vendorPhone
        vendorType
        vendorNote
        startDate
        vendorRate
        vendorIssue{
            id
            vendorIssueName
            vendorLabels
            vendorIssueDescription
            vendorIssueStatus
            vendorID
            isDeadline
            vendorLabels
            isDate
            isDateDone
            issueDirection
            authorAssign{
                id
                email
                name
            }
            total
        }
    }
}`;

export const ALL_VENDOR_QUERY = gql` 
query AllVendor($id: String!){
    AllVendor(id: $id){
        id
        vendorName
        vendorInfo
        vendorType
        vendorDauMoi
        vendorPhone
        vendorType
        vendorNote
        startDate
        vendorRate
        total
        vendorIssue{
            id
            vendorIssueName
            vendorIssueStatus
            vendorIssueDescription
            vendorLabels
            isDate
            issueDirection
            # author{
            #     email
            #     name
            # }
            # authorAssign{
            #     id
            #     email
            #     name
            # }
        }
    }
}`;


// export const ALL_VENDOR_QUERY_KANBAN = gql` 
// query AllVendorKanban{
//     AllVendor{
//         id
//         vendorName
//         vendorInfo
//         vendorType
//         vendorDauMoi
//         vendorPhone
//         vendorType
//         vendorNote
//         startDate
//         vendorRate
//         vendorIssue{
//             id
//             vendorIssueName
//             vendorIssueStatus
//             vendorIssueDescription
//             vendorLabels
//             isDate
//             author{
//                 email
//                 name
//             }
//             authorAssign{
//                 id
//                 email
//                 name
//             }
//         }
//     }
// }`;

export const DELETE_VENDOR = gql`
mutation deleteVendor($id:String!){
    deleteVendor(id: $id){
        message
        type
    }
}
`;

export const CREATE_VENDOR = gql`
mutation addVendor($vendorName: String!, $vendorInfo: String!, $vendorDauMoi: String!, $vendorPhone: String!, $vendorType: vendorTypeEnum!, $vendorNote: String!, $startDate: String!, $vendorRate: rateTypeEnum!){
addVendor(vendorName: $vendorName, vendorInfo: $vendorInfo, vendorDauMoi: $vendorDauMoi, vendorPhone: $vendorPhone, vendorType: $vendorType, vendorNote: $vendorNote, startDate: $startDate, vendorRate: $vendorRate){
        id
        vendorName
        vendorInfo
        vendorDauMoi
        vendorPhone
        vendorType
        vendorNote
        startDate
        vendorRate
    }
}
`;

export const UPDATE_VENDOR = gql`
mutation updateVendor($id: String!, $vendorName: String!, $vendorInfo: String!, $vendorDauMoi: String!, $vendorPhone: String!, $vendorType: vendorTypeEnum!, $vendorNote: String!, $startDate: String!, $vendorRate: rateTypeEnum!){
updateVendor(id: $id, vendorName: $vendorName, vendorInfo: $vendorInfo, vendorDauMoi: $vendorDauMoi, vendorPhone: $vendorPhone, vendorType: $vendorType, vendorNote: $vendorNote, startDate: $startDate, vendorRate: $vendorRate){
        id
        vendorName
        vendorInfo
        vendorDauMoi
        vendorPhone
        vendorType
        vendorNote
        startDate
        vendorRate
    }
}
`;