import gql from 'graphql-tag';
const cookieparser = process.server ? require('cookieparser') : undefined

export const state = () => {
  return {
    auth: null
  }
}
export const mutations = {
  setAuth (state, auth) {
    state.auth = auth
  }
}

const QUERY_USER = gql`
query user{
  me{
    id
    name
    email
  }
}`;

export const actions = {
  async nuxtServerInit ({ commit }, { app, req }) {
    const client = app.apolloProvider.clients.defaultClient;
    // let auth = null;
    const res = await client.query({
      query: QUERY_USER,
    }).catch(({graphQLErrors}) => {
        console.log('Unauthenticated');
    });
    if (res) {
      const {data} = res;
      if (data.me) {
        commit('setAuth', data.me);
      }
    }
    // if (req.headers.cookie) {
    //   const parsed = cookieparser.parse(req.headers.cookie)
    //   try {
    //     auth = JSON.parse(parsed.auth);
    //     commit('setAuth', auth);
    //   } catch (err) {
    //     if (res) {
    //       const {data} = res;
    //       if (data.profile) {
    //         commit('setAuth', data.profile);
    //       }
    //     }
    //   }
    // }
  }
}